const Sequelize = require('sequelize');

const directorModel = require ('.models/director');
const genreModel = require('./models/genre');
const movieModel = require('./models/movie');
const actorModel = require('./models/actor');
const movieActorModel = require('./models/movieActor');
const memberModel = require('./models/member');


/*
    1. Nombre de la base de datos
    2. Usuario de la base de datos
    3. Contraseña de la base de datos
    4. Objeto de configuración ORM

*/

const sequelize = new Sequelize('video-club', 'root', 'hhq5q4c9', {
    host: '127.0.0.1',
    dialect: 'mysql'
});

const Director = directorModel(sequelize, Sequelize);
const Genre = genreModel(sequelize, Sequelize);
const Movie = movieModel(sequelize, Sequelize);
const Actor = actorModel(sequelize, Sequelize);
const MovieActor = movieActorModel(sequelize, Sequelize);
const Member = memberModel(sequelize, Sequelize);


// Un género puede tener muchas películas
Genre.hasMany(Movie, {as: 'movies'});

// Una película tiene un género
Movie.belongsTo(Genre, {as: 'genre'})

//Un directorn puede tener muchas películas
Director.hasMany(Movie, {as:'movies'});

//Una película tiene un director
Movie.belongsTo(Director, {as: 'director'});

// Un actor participa en muchas peliculas
MovieActor.belongsTo(Movie, {foreignKey: 'movieId'});


// En una película participan muchos actores
MovieActor.belongsTo(Actor, {foreignKey: 'actorId'});

MovieActor.belongsToMany(Actor, {
    foreignKey: 'actorID',
    as: 'actors',
    throught: 'movies_actors'
});

Actor.belongsToMany(Movie, {
    foreignKey: 'movieId',
    as: 'movies',
    throught: 'movies_actors'
});


sequelize.sync({
    force:true
}).then(() => {
    console.log('Base de datos sicronizada.')
});

module.exports = (Director, Genre, Movie, Actor, Member);